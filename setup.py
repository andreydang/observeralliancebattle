from setuptools import setup

install_requires = [
    'requests'
]

setup(
    name='observer_alliance',
    install_requires=install_requires,
    version='1',
    packages=[''],
    url='',
    license='',
    author='andre',
    author_email='',
    description=''
)
