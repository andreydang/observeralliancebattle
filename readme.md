Install
-------

```
$ python -m venv .venv
$ .venv\Scripts\activate
$ pip install -e .
```

Run
-------

```
$ .venv\Scripts\python.exe observer_alliance.py
```