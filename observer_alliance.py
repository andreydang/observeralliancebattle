from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List
from time import sleep
import requests

BASE_URL = 'https://gameinfo.albiononline.com/api/gameinfo'


class Subject(ABC):
    """
    Интферфейс издателя объявляет набор методов для управлениями подпискичами.
    """

    def __init__(self):
        self.change_match_id = None

    @abstractmethod
    def attach(self, observer: Observer) -> None:
        """
        Присоединяет наблюдателя к издателю.
        """
        pass

    @abstractmethod
    def detach(self, observer: Observer) -> None:
        """
        Отсоединяет наблюдателя от издателя.
        """
        pass

    @abstractmethod
    def notify(self) -> None:
        """
        Уведомляет всех наблюдателей о событии.
        """
        pass


class ConcreteSubject(Subject):
    """
    Издатель владеет некоторым важным состоянием и оповещает наблюдателей о его
    изменениях.
    """
    _id_match: int = 0
    _state: bool = False
    change_match_id: bool = False
    array_match_id = []
    """
    Для удобства в этой переменной хранится состояние Издателя, необходимое всем
    подписчикам.
    """

    _observers: List[Observer] = []
    """
    Список подписчиков. В реальной жизни список подписчиков может храниться в
    более подробном виде (классифицируется по типу события и т.д.)
    """

    def attach(self, observer: Observer) -> None:
        print("Наблюдатель за API Alliance: На мои обновления подписались .")
        self._observers.append(observer)

    def detach(self, observer: Observer) -> None:
        self._observers.remove(observer)

    """
    Методы управления подпиской.
    """

    def notify(self) -> None:
        """
        Запуск обновления в каждом подписчике.
        """

        print("Наблюдатель за API Alliance: Нотфицирую своих подписчиков")
        for observer in self._observers:
            observer.update(self)

    def alliance_located_in_json(self, alliance_name, limit, period) -> None:
        """
        Функция проверяет нахождение альянса в последних матчах, \
        и если матчи поменялсь то мы уведомим подписчиков .
        """

        self.array_match_id = []
        while True:
            self.change_match_id: bool = False
            array_match_id_current = []
            response = requests.get(f'{BASE_URL}/battles', params={'limit': limit, 'sort': 'recent'}).json()
            array_match_id_current = self.add_match_id_with_alliance(alliance_name, array_match_id_current, response)
            self.notify_if_exist_diff(array_match_id_current)
            sleep(period)

    def notify_if_exist_diff(self, array_match_id_current):
        """ Сравнение массива который есть, с тем который к нам пришел
            в нем хранятся уникальные отсротированные значения матчей,
            если есть различия то нотифицирую подписчиков .

        :param array_match_id_current: массив который к нам пришел
        """

        if self.array_match_id != array_match_id_current:
            self.array_match_id = array_match_id_current
            self.change_match_id: bool = True
            self.notify()

    def add_match_id_with_alliance(self, alliance_name, array_match_id_current, response):
        """Добавление id матчей если альянс, который я передал в alliance_name есть в json-e

        :param response: собственно сам ответ от сервера, их которого тянем json
        :param alliance_name: имя альянса
        :param array_match_id_current: массив, который я буду наполнять id - шниками
        """
        for match in response:
            for id_alliance in (match['alliances']):
                self._id_match = int(match['id']) if alliance_name in match['alliances'][id_alliance]['name'] else 0
                self._state = True if self._id_match != 0 else False
                if self._state is True:
                    array_match_id_current.append(self._id_match)
        array_match_id_current = sorted(set(array_match_id_current))
        return array_match_id_current


class Observer(ABC):
    """
    Интерфейс Наблюдателя объявляет метод уведомления, который издатели
    используют для оповещения своих подписчиков.
    """

    @abstractmethod
    def update(self, subject: Subject) -> None:
        """
        Получить обновление от субъекта.
        """
        pass


"""
Конкретные Наблюдатели реагируют на обновления, выпущенные Издателем, к которому
они прикреплены.
"""


class ObserverLocatedAlliance(Observer):
    def update(self, subject: Subject) -> None:
        if subject.change_match_id:
            print("Смена id матчей :")
            print(subject.array_match_id)


if __name__ == "__main__":

    subject = ConcreteSubject()
    observer_alliance = ObserverLocatedAlliance()
    subject.attach(observer_alliance)
    subject.alliance_located_in_json('SQUAD', 50, 10)
